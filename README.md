## GamePlay - Next Level Week Together - Node Version - v14.17.0

<br>

## Screen

![image](https://gitlab.com/rodineiti/nextlevelweek-react-native-game-play/uploads/dbc7f6d540175cb091590134b3875ea3/cover.png)

## 💻 Projeto

Aplicativo para lhe ajudar a conectar-se e organiza o momento de diversão e jogar com os amigos. Crie grupos para jogar seus games favoritos com seus amigos com esse App que possui autenticação com Discord.

Este é um projeto desenvolvido durante a **[Next Level Week Together](https://nextlevelweek.com/)**, apresentada dos dias 20 a 27 de Junho de 2021.

## :hammer_and_wrench: Features

- [ ] Autenticação Social OAuth2 com servidor do Discord.
- [ ] Obtém perfil do usuário cadastro no Discord (username e avatar);
- [ ] Lista os servidores do Discord que o usuário faz parte;
- [ ] Permite realizar o agendamento de partidas;
- [ ] Permite filtrar as partidas por categoria;
- [ ] Exibe se a partida foi agendada em um servidor próprio (anfitrião) ou em servidores de outros (convidado);
- [ ] Compartilha o convite para ingressar no servidor do usuário;
- [ ] Permite redirecionar o usuário para o seu próprio servidor;
- [ ] Disponibiliza a função de Logout.

## ✨ Tecnologias

- [ ] React Native
- [ ] Typescript
- [ ] Expo
- [ ] Context API
- [ ] Async Storage
- [ ] Vector Icons
- [ ] React Native Svg e Svg Transform
- [ ] Axios
- [ ] Gradient colors
- [ ] OAuth2 Discord
- [ ] Expo Google Fonts
- [ ] React Navigation Stack
- [ ] React Native Gesture Handler
- [ ] Expo Authentication
- [ ] React Native Share
- [ ] Deep Link

## 🚀 Como executar

Clone o projeto e acesse a pasta do mesmo.

```bash
$ git clone https://gitlab.com/rodineiti/nextlevelweek-react-native-game-play
$ cd nextlevelweek-react-native-game-play
```

Para iniciá-lo, siga os passos abaixo:

```bash
# Instalar as dependências
$ yarn or npm install

# Iniciar o projeto
$ expo start
```

Lembre-se de criar o seu App no servidor do Discord para obter as credencias de autenticação. Em seguida, defina no arquivo .env as configurações do seu App (remova o example do arquivo .env.example).

```cl
REDIRECT_URI=
SCOPE=
RESPONSE_TYPE=
CLIENT_ID=
CDN_IMAGE=
```

## 🔖 Layout

Você pode visualizar o layout do projeto através do link abaixo:

- [Layout Web](https://www.figma.com/file/0kv33XYjvOgvKGKHBaiR07/GamePlay-NLW-Together?node-id=58913%3A83)

Lembrando que você precisa ter uma conta no [Figma](http://figma.com/).

## 📝 License

Esse projeto está sob a licença MIT.
