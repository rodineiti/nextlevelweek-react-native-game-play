import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { useAuth } from "../hooks/auth";
import { AppRoutes } from "./app.routes";
import { Login } from "../screens/Login";

export function Route() {
  const { user } = useAuth();
  return (
    <NavigationContainer>
      {user.id ? <AppRoutes /> : <Login />}
    </NavigationContainer>
  );
}
