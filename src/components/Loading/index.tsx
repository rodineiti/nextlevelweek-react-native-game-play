import React from "react";

import { StyleSheet, View, ActivityIndicator } from "react-native";
import { theme } from "../../global/styles/theme";

export function Loading() {
  return (
    <View style={styled.container}>
      <ActivityIndicator size="large" color={theme.colors.primary} />
    </View>
  );
}

const styled = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
