import React from "react";
import { StyleSheet, ScrollView } from "react-native";
import { theme } from "../../global/styles/theme";
import { categories } from "../../utils/categories";
import { Category } from "../Category";

type CategorySelectedProps = {
  categorySelected: string;
  setCategory: (categoryId: string) => void;
  hasCheckbox?: boolean;
};

export function CategorySelect({
  categorySelected,
  setCategory,
  hasCheckbox = false,
}: CategorySelectedProps) {
  return (
    <ScrollView
      horizontal
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{ paddingRight: 40 }}
      style={styled.container}
    >
      {categories.map((category) => (
        <Category
          key={category.id}
          title={category.title}
          icon={category.icon}
          checked={category.id === categorySelected}
          onPress={() => setCategory(category.id)}
          hasCheckbox={hasCheckbox}
        />
      ))}
    </ScrollView>
  );
}

const styled = StyleSheet.create({
  container: {
    minHeight: 120,
    maxHeight: 120,
    paddingLeft: 24,
  },
});
