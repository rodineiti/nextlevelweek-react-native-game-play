import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { theme } from "../../global/styles/theme";

type ListHeaderProps = {
  title: string;
  subtitle: string;
};

export function ListHeader({ title, subtitle }: ListHeaderProps) {
  return (
    <View style={styled.container}>
      <Text style={styled.title}>{title}</Text>
      <Text style={styled.subtitle}>{subtitle}</Text>
    </View>
  );
}

const styled = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 24,
    marginTop: 27,
  },
  title: {
    fontFamily: theme.fonts.title700,
    color: theme.colors.heading,
    fontSize: 18,
  },
  subtitle: {
    fontFamily: theme.fonts.text400,
    color: theme.colors.highlight,
    fontSize: 13,
  },
});
