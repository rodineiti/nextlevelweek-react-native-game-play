import React from "react";

import { StyleSheet, View } from "react-native";
import { theme } from "../../global/styles/theme";

type Props = {
  isCentered?: boolean;
};

export function Divider({ isCentered }: Props) {
  return (
    <View
      style={[
        styled.container,
        isCentered
          ? {
              marginVertical: 12,
            }
          : {
              marginTop: 2,
              marginBottom: 31,
            },
      ]}
    />
  );
}

const styled = StyleSheet.create({
  container: {
    height: 1,
    width: "78%",
    alignSelf: "flex-end",
    backgroundColor: theme.colors.secondary40,
  },
});
