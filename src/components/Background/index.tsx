import React, { ReactNode } from "react";
import { StyleSheet } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

import { theme } from "../../global/styles/theme";

type BackgroundProps = {
  children: ReactNode;
};

export function Background({ children }: BackgroundProps) {
  const { secondary80, secondary100 } = theme.colors;
  return (
    <LinearGradient
      style={styled.container}
      colors={[secondary80, secondary100]}
    >
      {children}
    </LinearGradient>
  );
}

const styled = StyleSheet.create({
  container: {
    flex: 1,
  },
});
