import React from "react";
import { RectButton, RectButtonProps } from "react-native-gesture-handler";
import { StyleSheet, Text, Image, View } from "react-native";
import { theme } from "../../global/styles/theme";

import discordImg from "./../../assets/discord.png";

type ButtonIconProps = RectButtonProps & {
  title: string;
  enableIcon?: boolean;
};

export function ButtonIcon({
  title,
  enableIcon = true,
  ...rest
}: ButtonIconProps) {
  return (
    <RectButton style={styled.container} {...rest}>
      {enableIcon && (
        <View style={styled.iconWrapper}>
          <Image source={discordImg} style={styled.icon} />
        </View>
      )}
      <Text style={styled.title}>{title}</Text>
    </RectButton>
  );
}

const styled = StyleSheet.create({
  container: {
    width: "100%",
    height: 56,
    backgroundColor: theme.colors.primary,
    borderRadius: 8,
    flexDirection: "row",
    alignItems: "center",
  },
  iconWrapper: {
    width: 56,
    height: 56,
    justifyContent: "center",
    alignItems: "center",
    borderRightWidth: 1,
    borderColor: theme.colors.line,
  },
  icon: {
    width: 24,
    height: 18,
  },
  title: {
    flex: 1,
    color: theme.colors.heading,
    fontFamily: theme.fonts.text500,
    fontSize: 15,
    textAlign: "center",
  },
});
