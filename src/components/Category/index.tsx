import React from "react";
import { SvgProps } from "react-native-svg";
import { LinearGradient } from "expo-linear-gradient";
import { StyleSheet, View, Text } from "react-native";
import { RectButton, RectButtonProps } from "react-native-gesture-handler";
import { theme } from "../../global/styles/theme";

type CategoryProps = RectButtonProps & {
  title: string;
  icon: React.FC<SvgProps>;
  checked?: boolean;
  hasCheckbox?: boolean;
};

export function Category({
  title,
  icon: Icon,
  checked = false,
  hasCheckbox = false,
  ...rest
}: CategoryProps) {
  const { secondary50, secondary70, secondary85, secondary40 } = theme.colors;
  return (
    <RectButton {...rest}>
      <LinearGradient
        style={styled.container}
        colors={[secondary50, secondary70]}
      >
        <LinearGradient
          style={[styled.content, { opacity: checked ? 1 : 0.5 }]}
          colors={[checked ? secondary85 : secondary50, secondary40]}
        >
          {hasCheckbox && (
            <View style={checked ? styled.checked : styled.check} />
          )}
          <Icon width={48} height={48} />
          <Text style={styled.title}>{title}</Text>
        </LinearGradient>
      </LinearGradient>
    </RectButton>
  );
}

const styled = StyleSheet.create({
  container: {
    width: 104,
    height: 120,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    marginRight: 8,
  },
  content: {
    width: 100,
    height: 116,
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 20,
  },
  check: {
    position: "absolute",
    top: 7,
    right: 7,
    width: 12,
    height: 12,
    backgroundColor: theme.colors.secondary100,
    borderColor: theme.colors.secondary50,
    borderWidth: 2,
    borderRadius: 3,
  },
  checked: {
    position: "absolute",
    top: 7,
    right: 7,
    width: 10,
    height: 10,
    backgroundColor: theme.colors.primary,
    borderRadius: 3,
  },
  title: {
    fontFamily: theme.fonts.title700,
    color: theme.colors.heading,
    fontSize: 15,
    marginTop: 15,
  },
});
