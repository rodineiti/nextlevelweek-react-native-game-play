import React from "react";
import { Alert, StyleSheet, Text, View } from "react-native";
import { RectButton } from "react-native-gesture-handler";
import { theme } from "../../global/styles/theme";
import { useAuth } from "../../hooks/auth";
import { Avatar } from "../Avatar";

export function Profile() {
  const { user, logout } = useAuth();

  function handleLogout() {
    Alert.alert("Logout", "Deseja sair do GamePlay?", [
      {
        text: "Não",
        style: "cancel",
      },
      {
        text: "Sim",
        onPress: () => logout(),
      },
    ]);
  }

  return (
    <View style={styled.container}>
      <RectButton onPress={handleLogout}>
        <Avatar urlImage={user.avatar} />
      </RectButton>
      <View>
        <View style={styled.user}>
          <Text style={styled.greeting}>Olá</Text>
          <Text style={styled.username}>{user.firstName}</Text>
        </View>
        <Text style={styled.message}>Hoje é dia de vitória</Text>
      </View>
    </View>
  );
}

const styled = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
  },
  user: {
    flexDirection: "row",
  },
  greeting: {
    fontFamily: theme.fonts.title500,
    fontSize: 24,
    color: theme.colors.heading,
    marginRight: 6,
  },
  username: {
    fontFamily: theme.fonts.title700,
    fontSize: 24,
    color: theme.colors.heading,
  },
  message: {
    fontFamily: theme.fonts.text400,
    color: theme.colors.highlight,
  },
});
