import React from "react";
import { LinearGradient } from "expo-linear-gradient";
import { StyleSheet, Image } from "react-native";
import { theme } from "../../global/styles/theme";

type AvatarProps = {
  urlImage: string;
};

export function Avatar({ urlImage }: AvatarProps) {
  const { secondary50, secondary70 } = theme.colors;
  return (
    <LinearGradient
      style={styled.container}
      colors={[secondary50, secondary70]}
    >
      <Image source={{ uri: urlImage }} style={styled.avatar} />
    </LinearGradient>
  );
}

const styled = StyleSheet.create({
  container: {
    width: 49,
    height: 49,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 22,
  },
  avatar: {
    width: 46,
    height: 46,
    borderRadius: 8,
  },
});
