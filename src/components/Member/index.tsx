import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { theme } from "../../global/styles/theme";

import { Avatar } from "../Avatar";

export type MemberProps = {
  id: string;
  username: string;
  avatar_url: string;
  status: string;
};

type Props = {
  data: MemberProps;
};

export function Member({ data }: Props) {
  const isOnline = data.status === "online";
  const { on, primary } = theme.colors;
  return (
    <View style={styled.container}>
      <Avatar urlImage={data.avatar_url} />

      <View>
        <Text style={styled.username}>{data.username}</Text>
        <View style={styled.status}>
          <View
            style={[
              styled.bullet,
              {
                backgroundColor: isOnline ? on : primary,
              },
            ]}
          />
          <Text style={styled.statusName}>
            {isOnline ? "Disponível" : "Ocupado"}
          </Text>
        </View>
      </View>
    </View>
  );
}

const styled = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
  },
  username: {
    fontFamily: theme.fonts.title700,
    color: theme.colors.heading,
    fontSize: 18,
  },
  status: {
    flexDirection: "row",
    alignItems: "center",
  },
  statusName: {
    fontFamily: theme.fonts.text400,
    color: theme.colors.highlight,
    fontSize: 13,
  },
  bullet: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginRight: 9,
  },
});
