import React from "react";
import { StyleSheet, Image, View } from "react-native";
import { theme } from "../../global/styles/theme";
import DiscordSvg from "../../assets/discord.svg";

const { CDN_IMAGE } = process.env;

type Props = {
  guildId: string;
  iconId: string | null;
};

export function GuildIcon({ guildId, iconId }: Props) {
  const uri = `${CDN_IMAGE}/icons/${guildId}/${iconId}.png`;
  return (
    <View style={styled.container}>
      {iconId ? (
        <Image source={{ uri }} style={styled.image} resizeMode="cover" />
      ) : (
        <DiscordSvg width={40} height={40} />
      )}
    </View>
  );
}

const styled = StyleSheet.create({
  container: {
    width: 62,
    height: 66,
    borderRadius: 8,
    backgroundColor: theme.colors.discord,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
  },
  image: {
    width: 62,
    height: 66,
    borderRadius: 8,
  },
});
