import React, { useState, useCallback } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation, useFocusEffect } from "@react-navigation/native";
import { getStatusBarHeight } from "react-native-iphone-x-helper";
import { StyleSheet, View, FlatList, Text } from "react-native";
import { Background } from "../../components/Background";
import { Profile } from "../../components/Profile";
import { ButtonAdd } from "../../components/ButtonAdd";
import { CategorySelect } from "../../components/CategorySelect";
import { ListHeader } from "../../components/ListHeader";
import { Appointment, AppointmentProps } from "../../components/Appointment";
import { Divider } from "../../components/Divider";
import { Loading } from "../../components/Loading";
import { COLLECTION_APPOINTMENTS } from "../../configs/database";

export function Home() {
  const [category, setCategory] = useState("");
  const [loading, setLoading] = useState(true);
  const navigation = useNavigation();
  const [appoitments, setAppoitments] = useState<AppointmentProps[]>([]);

  function handleCategorySelect(categoryId: string) {
    categoryId === category ? setCategory("") : setCategory(categoryId);
  }

  function handleDetails(guild: AppointmentProps) {
    navigation.navigate("AppointmentDetails", {
      guild,
    });
  }

  function handleCreate() {
    navigation.navigate("AppointmentCreate");
  }

  async function loadItems() {
    const storage = await AsyncStorage.getItem(COLLECTION_APPOINTMENTS);
    const data: AppointmentProps[] = storage ? JSON.parse(storage) : [];

    if (category) {
      setAppoitments(data.filter((item) => item.category === category));
    } else {
      setAppoitments(data);
    }

    setLoading(false);
  }

  useFocusEffect(
    useCallback(() => {
      loadItems();
    }, [category])
  );

  return (
    <Background>
      <View style={styled.header}>
        <Profile />
        <ButtonAdd onPress={handleCreate} />
      </View>
      <CategorySelect
        categorySelected={category}
        setCategory={handleCategorySelect}
      />

      {loading ? (
        <Loading />
      ) : (
        <>
          <ListHeader
            title={"Partidas agendadas"}
            subtitle={`Total ${appoitments.length}`}
          />

          <FlatList
            data={appoitments}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => (
              <Appointment data={item} onPress={() => handleDetails(item)} />
            )}
            style={styled.matches}
            showsVerticalScrollIndicator={false}
            ItemSeparatorComponent={() => <Divider />}
            contentContainerStyle={{ paddingBottom: 69 }}
          />
        </>
      )}
    </Background>
  );
}

const styled = StyleSheet.create({
  header: {
    width: "100%",
    paddingHorizontal: 24,
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: getStatusBarHeight() + 26,
    marginBottom: 32,
  },
  matches: {
    marginTop: 24,
    marginLeft: 24,
  },
});
