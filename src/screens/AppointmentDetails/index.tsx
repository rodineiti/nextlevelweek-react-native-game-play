import React, { useState, useEffect } from "react";
import { useRoute } from "@react-navigation/native";
import { BorderlessButton } from "react-native-gesture-handler";
import { Fontisto } from "@expo/vector-icons";
import * as Linking from "expo-linking";

import {
  StyleSheet,
  ImageBackground,
  View,
  Text,
  FlatList,
  Alert,
  Share,
  Platform,
} from "react-native";

import { theme } from "../../global/styles/theme";
import BannerImg from "../../assets/banner.png";

import { Background } from "../../components/Background";
import { Header } from "../../components/Header";
import { ListHeader } from "../../components/ListHeader";
import { Member, MemberProps } from "../../components/Member";
import { Divider } from "../../components/Divider";
import { ButtonIcon } from "../../components/ButtonIcon";
import { getBottomSpace } from "react-native-iphone-x-helper";
import { AppointmentProps } from "../../components/Appointment";
import { Loading } from "../../components/Loading";
import { api } from "../../services/api";

type ParamsProps = {
  guild: AppointmentProps;
};

type WidgetProps = {
  id: string;
  name: string;
  instant_invite: string;
  members: MemberProps[];
};

export function AppointmentDetails() {
  const [widget, setWidget] = useState<WidgetProps>({} as WidgetProps);
  const [loading, setLoading] = useState(true);
  const route = useRoute();
  const { guild } = route.params as ParamsProps;

  async function fetchGuildWidget() {
    try {
      const response = await api.get(`/guilds/${guild.guild.id}/widget.json`);
      setWidget(response.data);
    } catch (error) {
      console.log("DEU MUITO ERRO AO TENTAR RESGATAR WIDGETS", error);
      Alert.alert(
        "Não foi possível resgatar as informações, verifique se o Widget está ativo no servidor"
      );
    } finally {
      setLoading(false);
    }
  }

  function hadleShareInvite() {
    const message =
      Platform.OS === "ios"
        ? `Junte-se a ${guild.guild.name}`
        : widget.instant_invite;

    Share.share({
      message,
      url: widget.instant_invite,
    });
  }

  function handleOpenGuild() {
    if (widget.instant_invite) Linking.openURL(widget.instant_invite);
  }

  useEffect(() => {
    fetchGuildWidget();
  }, []);

  return (
    <Background>
      <Header
        title={"Detalhes"}
        action={
          guild.guild.owner &&
          widget.instant_invite && (
            <BorderlessButton onPress={hadleShareInvite}>
              <Fontisto name="share" size={24} color={theme.colors.primary} />
            </BorderlessButton>
          )
        }
      />

      <ImageBackground source={BannerImg} style={styled.banner}>
        <View style={styled.bannerContent}>
          <Text style={styled.title}>{guild.guild.name}</Text>
          <Text style={styled.subtitle}>{guild.description}</Text>
        </View>
      </ImageBackground>

      {loading ? (
        <Loading />
      ) : (
        <>
          <ListHeader
            title={"Jogadores"}
            subtitle={`Total ${widget.members.length}`}
          />

          <FlatList
            data={widget.members}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => <Member key={item.id} data={item} />}
            ItemSeparatorComponent={() => <Divider isCentered />}
            style={styled.members}
          />
        </>
      )}

      {guild.guild.owner && widget.instant_invite && (
        <View style={styled.wrapperButton}>
          <ButtonIcon
            title={"Entrar na partida"}
            enableIcon={true}
            onPress={handleOpenGuild}
          />
        </View>
      )}
    </Background>
  );
}

const styled = StyleSheet.create({
  banner: {
    width: "100%",
    height: 234,
  },
  bannerContent: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 24,
    marginBottom: 30,
  },
  title: {
    fontSize: 28,
    fontFamily: theme.fonts.title700,
    color: theme.colors.heading,
  },
  subtitle: {
    fontSize: 13,
    fontFamily: theme.fonts.text400,
    color: theme.colors.heading,
    lineHeight: 21,
  },
  members: {
    marginLeft: 24,
    marginTop: 27,
  },
  wrapperButton: {
    paddingHorizontal: 24,
    paddingVertical: 20,
    marginBottom: getBottomSpace(),
  },
});
