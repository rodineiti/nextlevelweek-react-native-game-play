import React, { useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { Feather } from "@expo/vector-icons";
import uuid from "react-native-uuid";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Alert,
} from "react-native";
import { RectButton } from "react-native-gesture-handler";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { theme } from "../../global/styles/theme";

import { COLLECTION_APPOINTMENTS } from "../../configs/database";
import { CategorySelect } from "../../components/CategorySelect";
import { Header } from "../../components/Header";
import { GuildIcon } from "../../components/GuildIcon";
import { SmallInput } from "../../components/SmallInput";
import { TextArea } from "../../components/TextArea";
import { ButtonIcon } from "../../components/ButtonIcon";
import { ModalView } from "../../components/ModalView";
import { Guilds } from "../Guilds";
import { GuildProps } from "../../components/Guild";
import { Background } from "../../components/Background";

export function AppointmentCreate() {
  const navigation = useNavigation();
  const [category, setCategory] = useState("");
  const [guild, setGuild] = useState<GuildProps>({} as GuildProps);
  const [openModal, setOpenModal] = useState(false);

  const [d, setD] = useState("");
  const [m, setM] = useState("");
  const [h, setH] = useState("");
  const [min, setMin] = useState("");
  const [description, setDescription] = useState("");

  function handleOpenModal() {
    setOpenModal(true);
  }

  function handleSelectGuild(guildSelect: GuildProps) {
    setGuild(guildSelect);
    setOpenModal(false);
  }

  function handleCloseModal() {
    setOpenModal(false);
  }

  function handleSelectCategory(categoryId: string) {
    setCategory(categoryId);
  }

  async function handleSave() {
    if (
      d.trim() === "" ||
      m.trim() === "" ||
      h.trim() === "" ||
      min.trim() === "" ||
      category.trim() === ""
    ) {
      Alert.alert("Favor, informe todos os campos");
      return;
    }

    const newItem = {
      id: uuid.v4(),
      guild,
      category,
      date: `${d}/${m} às ${h}:${min}h`,
      description,
    };

    const storage = await AsyncStorage.getItem(COLLECTION_APPOINTMENTS);
    const appointments = storage ? JSON.parse(storage) : [];

    await AsyncStorage.setItem(
      COLLECTION_APPOINTMENTS,
      JSON.stringify([...appointments, newItem])
    );

    navigation.navigate("Home");
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styled.container}
    >
      <Background>
        <ScrollView>
          <Header title={"Agendar partida"} />

          <Text
            style={[
              styled.label,
              { marginLeft: 24, marginTop: 36, marginBottom: 18 },
            ]}
          >
            Categoria
          </Text>

          <CategorySelect
            hasCheckbox
            categorySelected={category}
            setCategory={handleSelectCategory}
          />

          <View style={styled.form}>
            <RectButton onPress={handleOpenModal}>
              <View style={styled.select}>
                {guild.icon ? (
                  <GuildIcon guildId={guild.id} iconId={guild.icon} />
                ) : (
                  <View style={styled.image} />
                )}

                <View style={styled.selectBody}>
                  <Text style={styled.label}>
                    {guild.name ? guild.name : "Selecione um servidor"}
                  </Text>
                </View>

                <Feather
                  name="chevron-right"
                  color={theme.colors.heading}
                  size={28}
                />
              </View>
            </RectButton>

            <View style={styled.field}>
              <View>
                <Text style={[styled.label, { marginBottom: 12 }]}>
                  Dia e mês
                </Text>
                <View style={styled.column}>
                  <SmallInput maxLength={2} onChangeText={setD} value={d} />
                  <Text style={styled.divider}>/</Text>
                  <SmallInput maxLength={2} onChangeText={setM} value={m} />
                </View>
              </View>
              <View>
                <Text style={[styled.label, { marginBottom: 12 }]}>
                  Hora e minuto
                </Text>
                <View style={styled.column}>
                  <SmallInput maxLength={2} onChangeText={setH} value={h} />
                  <Text style={styled.divider}>:</Text>
                  <SmallInput maxLength={2} onChangeText={setMin} value={min} />
                </View>
              </View>
            </View>

            <View style={[styled.field, { marginBottom: 12 }]}>
              <Text style={styled.label}>Descrição</Text>
              <Text style={styled.limitChar}>Max. 100 caracteres</Text>
            </View>

            <TextArea
              multiline
              maxLength={100}
              numberOfLines={5}
              autoCorrect={false}
              onChangeText={setDescription}
              value={description}
            />

            <View style={styled.footer}>
              <ButtonIcon
                title={"Agendar"}
                enableIcon={false}
                onPress={handleSave}
              />
            </View>
          </View>
        </ScrollView>
      </Background>

      <ModalView visible={openModal} closeModal={handleCloseModal}>
        <Guilds handleGuildSelected={handleSelectGuild} />
      </ModalView>
    </KeyboardAvoidingView>
  );
}

const styled = StyleSheet.create({
  container: {
    flex: 1,
  },
  label: {
    fontSize: 18,
    fontFamily: theme.fonts.title700,
    color: theme.colors.heading,
  },
  form: {
    paddingHorizontal: 24,
    marginTop: 32,
  },
  select: {
    width: "100%",
    height: 68,
    flexDirection: "row",
    borderColor: theme.colors.secondary50,
    borderWidth: 1,
    borderRadius: 8,
    alignItems: "center",
    paddingRight: 25,
    overflow: "hidden",
  },
  selectBody: {
    flex: 1,
    alignItems: "center",
  },
  image: {
    width: 64,
    height: 68,
    backgroundColor: theme.colors.secondary40,
    borderColor: theme.colors.secondary50,
    borderWidth: 1,
    borderRadius: 8,
  },
  field: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 30,
  },
  divider: {
    marginRight: 4,
    fontSize: 18,
    fontFamily: theme.fonts.text500,
    color: theme.colors.highlight,
  },
  column: {
    flexDirection: "row",
    alignItems: "center",
  },
  limitChar: {
    fontSize: 13,
    fontFamily: theme.fonts.text400,
    color: theme.colors.heading,
  },
  footer: {
    marginVertical: 20,
    marginBottom: 56,
  },
});
