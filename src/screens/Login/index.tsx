import React from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  ActivityIndicator,
} from "react-native";
import { theme } from "../../global/styles/theme";

import { ButtonIcon } from "./../../components/ButtonIcon";
import IllustrationImg from "./../../assets/illustration.png";

import { Background } from "../../components/Background";
import { useAuth } from "../../hooks/auth";

export function Login() {
  const { loading, login } = useAuth();

  async function handleLogin() {
    try {
      await login();
    } catch (error) {
      Alert.alert(error);
    }
  }

  return (
    <Background>
      <View style={styled.container}>
        <Image
          source={IllustrationImg}
          style={styled.image}
          resizeMode="stretch"
        />
        <View style={styled.content}>
          <Text style={styled.title}>
            Conecte-se {`\n`}e organize suas {`\n`}jogatinas
          </Text>
          <Text style={styled.subtitle}>
            Crie grupos para jogar seus games {`\n`}favoritos com seus amigos
          </Text>

          {loading ? (
            <ActivityIndicator color={theme.colors.primary} />
          ) : (
            <ButtonIcon
              title={"Entrar com Discord"}
              enableIcon={true}
              onPress={handleLogin}
            />
          )}
        </View>
      </View>
    </Background>
  );
}

const styled = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: "100%",
    height: 360,
  },
  content: {
    marginTop: -40,
    paddingHorizontal: 50,
  },
  title: {
    color: theme.colors.heading,
    textAlign: "center",
    fontSize: 40,
    marginBottom: 10,
    fontFamily: theme.fonts.title700,
    lineHeight: 40,
  },
  subtitle: {
    color: theme.colors.heading,
    fontSize: 15,
    textAlign: "center",
    marginBottom: 64,
    fontFamily: theme.fonts.title500,
    lineHeight: 25,
  },
});
