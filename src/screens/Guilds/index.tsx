import React, { useState, useEffect } from "react";
import { StyleSheet, View, FlatList, Text } from "react-native";
import { Guild, GuildProps } from "../../components/Guild";
import { Divider } from "../../components/Divider";
import { Loading } from "../../components/Loading";
import { api } from "../../services/api";

type Props = {
  handleGuildSelected: (guild: GuildProps) => void;
};

export function Guilds({ handleGuildSelected }: Props) {
  const [guilds, setGuilds] = useState<GuildProps[]>([]);
  const [loading, setLoading] = useState(true);

  async function fetchGuilds() {
    try {
      const response = await api.get("/users/@me/guilds");
      setGuilds(response.data);
    } catch (error) {
      console.log("DEU MUITO ERRO NA LISTA DE GUILDS", error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchGuilds();
  }, []);

  return (
    <View style={styled.container}>
      {loading ? (
        <Loading />
      ) : (
        <FlatList
          data={guilds}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <Guild data={item} onPress={() => handleGuildSelected(item)} />
          )}
          showsVerticalScrollIndicator={false}
          ItemSeparatorComponent={() => <Divider isCentered />}
          ListHeaderComponent={() => <Divider isCentered />}
          style={styled.guilds}
          contentContainerStyle={{ paddingBottom: 69, paddingTop: 104 }}
        />
      )}
    </View>
  );
}

const styled = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingTop: 24,
  },
  guilds: {
    width: "100%",
  },
});
